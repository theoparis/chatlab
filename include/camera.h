#include "three/transform.h"
#include "three/app.h"
#include "glm/glm.hpp"

namespace chatlab::component {
    struct Ray {
            glm::vec3 position;
    };

    struct Camera {
            three::math::Transform transform;
            float fov;
            float near;
            float far;

            auto getCameraMatrix(three::App *app) -> glm::mat4;
            auto createRay(three::App *app) -> Ray;
    };
} // namespace chatlab::component
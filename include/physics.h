#pragma once
#include "spdlog/spdlog.h"
#include "three/three.h"
#include "glm/glm.hpp"
#include "entt/entt.hpp"

namespace chatlab::physics {
    using Transform = three::math::Transform;
    using Collision = std::tuple<bool, three::math::Direction, glm::vec3>;

    auto vectorDirection(glm::vec3 target) -> three::math::Direction;

    struct PhysicsBody {
            glm::vec3 velocity;
            glm::vec3 force;
            float mass;
            bool isColliding;
    };

    struct BoundingBox {
            glm::vec3 min;
            glm::vec3 max;

            auto isInside(glm::vec3 point) -> bool;
            auto isInside(BoundingBox other) -> bool;

            auto getCenter() -> glm::vec3;
    };

    struct PhysicsObject {
            Transform transform;
            PhysicsBody properties;
            BoundingBox box;
    };

    auto checkCollision(PhysicsObject &one, PhysicsObject &two) -> Collision;

    void updatePhysics(entt::registry &registry, glm::vec3 &gravity,
                       float groundLevel);

    void detectCollisions(entt::registry &registry);
} // namespace chatlab::physics

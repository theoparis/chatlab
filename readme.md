# Chatlab

Chatlab is a C++ game based on the [three.cpp engine](https://gitlab.com/theoparis/three-cpp).

## Building From Source

You will need [xmake](https://xmake.io), a C++17 compiler, [three](https://gitlab.com/theoparis/three-cpp), GLM, and GLFW installed on your system beforehand.

```bash
xmake
```

You can find the compiled binaries for your platform in the **build** folder.


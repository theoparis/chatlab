#include "camera.h"
#include "glm/gtx/quaternion.hpp"
#include <glm/ext/quaternion_common.hpp>

namespace chatlab::component {
    auto Camera::getCameraMatrix(three::App *app) -> glm::mat4 {
        auto windowSize = app->getWindowSize();

        return glm::perspective(fov, windowSize.x / windowSize.y, near, far) *
               transform.getMatrix();
    };

    auto Camera::createRay(three::App *app) -> Ray {
        auto windowSize = app->getWindowSize();
        auto mousePosition = app->getMousePosition();

        // these positions must be in range [-1, 1] (!!!), not [0, width] and
        // [0, height]
        auto mouseClip =
            glm::vec4(mousePosition.x / 2.0f / windowSize.x - 1,
                      1 - mousePosition.y * 2 / windowSize.y, 0, 1);

        auto proj =
            glm::perspective(fov, windowSize.x / windowSize.y, near, far);
        auto pickingPos = glm::inverse(proj) *
                          glm::inverse(transform.getMatrix()) * mouseClip;

        return {pickingPos};
    }
} // namespace chatlab::component

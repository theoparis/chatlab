#include "entt/entt.hpp"
#include "three-gui/imgui.h"
#include "fstream"
#include "sstream"
#include "string"
#include "spdlog/spdlog.h"
#include "physics.h"
#include "glm/gtx/string_cast.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <GLFW/glfw3.h>
#include <entt/entity/entity.hpp>
#include <iostream>
#include "fmt/format.h"
#include "three/mesh.h"
#include "camera.h"
#include "toml++/toml.h"

using Transform = chatlab::physics::Transform;
using PhysicsBody = chatlab::physics::PhysicsBody;

auto loadFile(std::string path) -> std::string {
    std::ifstream f(path);
    std::stringstream buffer;
    buffer << f.rdbuf();

    return buffer.str();
}

// TODO: player class
static bool cursorLocked = false;

auto main(int argc, char *argv[]) -> int {
    spdlog::set_level(spdlog::level::debug);

    // TODO: toml configuration
    auto config = toml::parse_file(argv[1]);

    auto app = new three::App(680, 460, "Chatlab");

    glfwSetKeyCallback(
        app->getWindow(),
        [](GLFWwindow *window, int key, int scancode, int action, int mode) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
                cursorLocked = !cursorLocked;
                glfwSetInputMode(window, GLFW_CURSOR,
                                 cursorLocked ? GLFW_CURSOR_DISABLED
                                              : GLFW_CURSOR_NORMAL);
            }
        });

    auto vertexShader =
        new three::Shader(loadFile("resources/default.vert"), GL_VERTEX_SHADER);
    auto fragmentShader = new three::Shader(loadFile("resources/default.frag"),
                                            GL_FRAGMENT_SHADER);

    auto shaderProgram = new three::ShaderProgram(vertexShader->getId(),
                                                  fragmentShader->getId());

    auto registry = entt::registry();

    // ground
    auto ground = registry.create();
    registry.emplace<three::Mesh>(ground, three::primitive::createCube());
    registry.emplace<Transform>(ground, glm::vec3{0.0, -2.0, 0.0},
                                glm::vec3{0.0, 0.0, 0.0},
                                glm::vec3{1.5, 0.2, 1.5});
    registry.emplace<PhysicsBody>(ground, glm::vec3{0.0, 0.0, 0.0},
                                  glm::vec3{0.0, 0.0, 0.0}, 0.0, false);
    registry.emplace<three::Material>(
        ground, glm::vec3(0.0, 0.0, 0.0),
        config["debug"]["wireframe"].as_boolean()->get());

    for (int i = 0; i < 20; i++) {
        auto entity = registry.create();
        registry.emplace<three::Mesh>(entity, three::primitive::createCube());
        registry.emplace<Transform>(entity);
        registry.emplace<three::Material>(
            entity, glm::vec3(1.0, 0.0, 0.0),
            config["debug"]["wireframe"].as_boolean()->get());
        registry.emplace<PhysicsBody>(entity, glm::vec3(), glm::vec3(), 1.0);
    }

    chatlab::component::Camera cam = {{glm::vec3(0.0f, 0.0f, -5.0f),
                                       glm::vec3(0.0f, 0.0f, 0.0f),
                                       glm::vec3(1.0f, 1.0f, 1.0f)},
                                      45.0f,
                                      0.01f,
                                      1000.0f};

    for (auto [entity, mesh] : registry.view<three::Mesh>().each()) {
        registry.emplace<three::MeshRenderer>(entity, mesh);
    }

    app->run([&shaderProgram, &app, &registry, &config,
              &cam](GLFWwindow *window) {
        auto windowSize = app->getWindowSize();
        ImGui::Begin("Entities");
        for (auto [entity, transform] : registry.view<Transform>().each()) {
            if (ImGui::TreeNode(
                    std::to_string(entt::to_entity(entity)).c_str())) {
                if (ImGui::CollapsingHeader("Transform")) {
                    ImGui::SliderFloat3("Position",
                                        glm::value_ptr(transform.position),
                                        -100.0f, 100.0f);
                    ImGui::SliderFloat3("Rotation",
                                        glm::value_ptr(transform.rotation),
                                        -180.0f, 180.0f);
                    ImGui::SliderFloat3(
                        "Scale", glm::value_ptr(transform.scale), 0.0f, 100.0f);
                }

                ImGui::TreePop();
            }
        }
        ImGui::End();

        // perform updates
        auto configGravity = config["world"]["gravity"].as_array();
        float speed = config["speed"].as_floating_point()->get();
        float mouseSensitivity =
            config["mouse_sensitivity"].as_floating_point()->get();

        glm::vec3 gravity = {configGravity->at(0).as_floating_point()->get(),
                             configGravity->at(1).as_floating_point()->get(),
                             configGravity->at(2).as_floating_point()->get()};
        chatlab::physics::updatePhysics(
            registry, gravity,
            config["world"]["ground_level"].as_floating_point()->get());
        chatlab::physics::detectCollisions(registry);

        if (glfwGetKey(app->getWindow(), GLFW_KEY_W) == GLFW_PRESS) {
            cam.transform.position += speed * glm::vec3(0.0, 0.0, 1.0);
        }

        if (glfwGetKey(app->getWindow(), GLFW_KEY_A) == GLFW_PRESS) {
            cam.transform.position +=
                speed * -glm::normalize(glm::cross(glm::vec3(0.0, 0.0, 1.0),
                                                   glm::vec3(0.0, 1.0, 0.0)));
        }

        if (glfwGetKey(app->getWindow(), GLFW_KEY_S) == GLFW_PRESS) {
            cam.transform.position += speed * -glm::vec3(0.0, 0.0, 1.0);
        }

        if (glfwGetKey(app->getWindow(), GLFW_KEY_D) == GLFW_PRESS) {
            cam.transform.position +=
                speed * glm::normalize(glm::cross(glm::vec3(0.0, 0.0, 1.0),
                                                  glm::vec3(0.0, 1.0, 0.0)));
        }

        if (glfwGetKey(app->getWindow(), GLFW_KEY_SPACE) == GLFW_PRESS) {
            cam.transform.position += speed * -glm::vec3(0.0, 1.0, 0.0);
        }

        if (glfwGetKey(app->getWindow(), GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
            cam.transform.position += speed * glm::vec3(0.0, 1.0, 0.0);
        }

        if (cursorLocked &&
            glfwGetMouseButton(app->getWindow(), GLFW_MOUSE_BUTTON_LEFT) ==
                GLFW_PRESS) {
            auto cursorPosition = app->getMousePosition();
            auto rot =
                glm::vec2(mouseSensitivity *
                              (float)(cursorPosition.y - (windowSize.y / 2.0)) /
                              windowSize.y,
                          mouseSensitivity *
                              (float)(cursorPosition.x - (windowSize.x / 2.0)) /
                              windowSize.x);
            glfwSetCursorPos(app->getWindow(), (app->getWindowSize().x / 2.0),
                             (app->getWindowSize().y / 2.0));

            cam.transform.rotation +=
                glm::vec3(rot.x, rot.y, cam.transform.rotation.z);
        }

        // render to the screen
        shaderProgram->bind();
        for (auto [entity, transform, meshRenderer, material] :
             registry.view<Transform, three::MeshRenderer, three::Material>()
                 .each()) {
            shaderProgram->setUniform("transform", cam.getCameraMatrix(app) *
                                                       transform.getMatrix());
            shaderProgram->setUniform("color", material.color);
            if (material.wireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            meshRenderer.draw();

            // spdlog::debug(glm::to_string(app->getMousePosition()));

            // spdlog::debug("Vertices: {}", meshRenderer.mesh.vertices.size());
            // glm::to_string(cam.getCameraMatrix(windowSize)));
            // spdlog::debug("Position: {}",
            // glm::to_string(transform.position));
            // auto ray = cam.createRay(app);
            // spdlog::debug("Raycast: {}",
            //   glm::to_string(ray.position));
        }
        shaderProgram->unbind();
    });

    return 0;
}

#include "physics.h"
#include <three/transform.h>
#include <vector>

namespace chatlab::physics {
    auto BoundingBox::isInside(glm::vec3 point) -> bool {
        return (point.x >= min.x && point.x <= max.x) &&
               (point.y >= min.y && point.y <= max.y) &&
               (point.z >= min.z && point.z <= max.z);
    }

    auto BoundingBox::isInside(BoundingBox other) -> bool {
        return (min.x <= other.max.x && max.x >= other.min.x) &&
               (min.y <= other.max.y && max.y >= other.min.y) &&
               (min.z <= other.max.z && max.z >= other.max.z);
    }

    auto BoundingBox::getCenter() -> glm::vec3 {
        auto center = glm::vec3(min);
        center += max;
        center *= .5f;

        return center;
    }

    auto vectorDirection(glm::vec3 target) -> three::math::Direction {
        std::vector<glm::vec3> compass = {
            glm::vec3(0.0f, 1.0f, 0.0f),  // up
            glm::vec3(1.0f, 0.0f, 0.0f),  // right
            glm::vec3(0.0f, -1.0f, 0.0f), // down
            glm::vec3(-1.0f, 0.0f, 0.0f), // left
            glm::vec3(0.0f, 0.0f, 1.0f),  // forward
            glm::vec3(0.0f, 0.0f, -1.0f), // backward
        };
        float max = 0.0f;
        unsigned int best_match = -1;
        for (unsigned int i = 0; i < 4; i++) {
            float dot_product = glm::dot(glm::normalize(target), compass[i]);
            if (dot_product > max) {
                max = dot_product;
                best_match = i;
            }
        }
        return (three::math::Direction)best_match;
    }

    void updatePhysics(entt::registry &registry, glm::vec3 &gravity,
                       float groundLevel) {
        auto view = registry.view<three::math::Transform, PhysicsBody>();

        for (auto &&[entity, transform, body] : view.each()) {
            if (body.mass > 0) {
                body.force += body.mass * gravity;

                if (body.isColliding || transform.position.y < groundLevel)
                    body.velocity =
                        glm::vec3(body.velocity.x, 0.0f, body.velocity.z);
                else
                    body.velocity += body.force / body.mass;
            } else {
                body.velocity += body.force;
            }

            transform.position += body.velocity;

            body.force = glm::vec3();
        }
    }

    auto checkCollision(PhysicsObject &a, PhysicsObject &b) -> Collision {
        // get center point circle first
        auto centerA = a.transform.position + a.box.getCenter();
        auto centerB = b.transform.position + b.box.getCenter();

        // get difference vector between both centers
        glm::vec3 difference = centerA - centerB;

        if (glm::length(difference) <= glm::length(b.box.max))
            return std::make_tuple(true, vectorDirection(difference),
                                   difference);
        else
            return std::make_tuple(false, three::math::Direction::UP,
                                   glm::vec3(0.0f, 0.0f, 0.0f));
    }

    void detectCollisions(entt::registry &registry) {
        auto view = registry.view<Transform, PhysicsBody>();

        for (auto [entityA, transformA, bodyA] : view.each()) {
            for (auto [entityB, transformB, bodyB] : view.each()) {
                if (entityA == entityB) {
                    break;
                }

                auto boxA = BoundingBox{transformA.position - transformA.scale,
                                        transformA.position + transformA.scale};
                auto boxB = BoundingBox{transformB.position - transformB.scale,
                                        transformB.position + transformB.scale};

                auto objA = PhysicsObject{transformA, bodyA, boxA};
                auto objB = PhysicsObject{transformB, bodyB, boxB};

                Collision collision = checkCollision(objA, objB);

                if (std::get<0>(collision)) {
                    // handle collision

                    auto dir = std::get<1>(collision);
                    auto diff_vector = std::get<2>(collision);

                    spdlog::debug("collision length: {}",
                                  glm::length(diff_vector));

                    // horizontal collision

                    bodyB.velocity += diff_vector;

                    bodyB.isColliding = true;
                } else {
                    bodyB.isColliding = false;
                }
            }
        }
    }
} // namespace chatlab::physics
